package com.company.calculator;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.*;

public class ExpressionEvaluatorVisitor extends ExpBaseVisitor<Integer> {

    private final Optional<ExpressionEvaluatorVisitor> parentVisitor;
    private final Map<String, Integer> environment;

    public ExpressionEvaluatorVisitor() {
        this(Optional.empty());
    }

    public ExpressionEvaluatorVisitor(Optional<ExpressionEvaluatorVisitor> parentVisitor) {
        this.parentVisitor = parentVisitor;
        this.environment = new HashMap<>();
    }

    @Override
    public Integer visitExpression(ExpParser.ExpressionContext ctx) {
        return visit(ctx.children.get(0));
    }

    @Override
    public Integer visitLetExp(ExpParser.LetExpContext ctx) {
        TerminalNode terminalNode = (TerminalNode) ctx.children.get(2);
        ExpParser.ExpressionContext variableValueExpression = (ExpParser.ExpressionContext) ctx.children.get(4);
        ExpParser.ExpressionContext expressionWithVariable = (ExpParser.ExpressionContext) ctx.children.get(6);

        String variableString = terminalNode.getSymbol().getText();
        environment.put(variableString, visit(variableValueExpression));

        return new ExpressionEvaluatorVisitor(Optional.of(this)).visit(expressionWithVariable);
    }

    @Override
    public Integer visitDivExp(ExpParser.DivExpContext ctx) {
        List<ExpParser.ExpressionContext> expressions = new ArrayList<>();
        for (ParseTree child : ctx.children) {
            if (child instanceof ExpParser.ExpressionContext) {
                expressions.add((ExpParser.ExpressionContext) child);
            }
        }
        if (expressions.size() != 2) {
            throw new RuntimeException("Expected 2 subexpressions  in " + ctx.toString());
        }
        return visit(expressions.get(0)) / visitExpression(expressions.get(1));
    }

    @Override
    public Integer visitIdExp(ExpParser.IdExpContext ctx) {
        TerminalNode terminalNode = (TerminalNode) ctx.children.get(0);
        return lookupVariable(terminalNode.getSymbol().getText());
    }

    private Integer lookupVariable(String variableString) {
        Integer value = environment.get(variableString);
        if (value == null) {
            if (parentVisitor.isPresent()) {
                return parentVisitor.get().lookupVariable(variableString);
            } else {
                throw new RuntimeException("Undefined variable " + variableString);
            }
        } else {
            return value;
        }
    }

    @Override
    public Integer visitMultExp(ExpParser.MultExpContext ctx) {
        List<ExpParser.ExpressionContext> expressions = new ArrayList<>();
        for (ParseTree child : ctx.children) {
            if (child instanceof ExpParser.ExpressionContext) {
                expressions.add((ExpParser.ExpressionContext) child);
            }
        }
        if (expressions.size() != 2) {
            throw new RuntimeException("Expected 2 subexpressions  in " + ctx.toString());
        }
        return visit(expressions.get(0)) * visitExpression(expressions.get(1));
    }

    @Override
    public Integer visitAddExp(ExpParser.AddExpContext ctx) {
        List<ExpParser.ExpressionContext> expressions = new ArrayList<>();
        for (ParseTree child : ctx.children) {
            if (child instanceof ExpParser.ExpressionContext) {
                expressions.add((ExpParser.ExpressionContext) child);
            }
        }
        if (expressions.size() != 2) {
            throw new RuntimeException("Expected 2 subexpressions  in " + ctx.toString());
        }
        return visit(expressions.get(0)) + visit(expressions.get(1));
    }

    @Override
    public Integer visitNumberExp(ExpParser.NumberExpContext ctx) {
        TerminalNode terminalNode = (TerminalNode) ctx.children.get(0);
        return Integer.parseInt(terminalNode.getSymbol().getText());
    }
}
