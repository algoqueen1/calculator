// Define a grammar called Hello
grammar Exp;

//grammar Hello;
//r  : 'hello' ID ;         // match keyword hello followed by an identifier

///* This will be the entry point of our parser. */
//eval
//    :    expression
//    ;

/* Addition and subtraction have the lowest precedence. */
expression
    :    addExp |
         multExp |
         numberExp |
         letExp |
         divExp |
         idExp |
    ;

letExp : 'let' '(' ID ',' expression ',' expression ')';


divExp
    :    'div'
         '(' expression ',' expression ')'
    ;

multExp
    :    'mult'
         '(' expression ',' expression ')'
    ;

numberExp : Number;

addExp
    :    'add'
         '(' expression ',' expression ')'
    ;

/* An expression atom is the smallest part of an expression: a number. Or
   when we encounter parenthesis, we're making a recursive call back to the
   rule 'additionExp'. As you can see, an 'atomExp' has the highest precedence. */
//atomExp
//    :    Number
//    ;

/* A number: can be an integer value, or a decimal value */
Number
    :    ('0'..'9')+
    ;

idExp : ID;

ID :  (('a'..'z')|('A'..'Z'))+;


/* We're going to ignore all white space characters */
//WS
//    :   (' ' | '\t' | '\r'| '\n') {$channel=HIDDEN;}
//    ;

//ID : [a-z]+ ;             // match lower-case identifiers
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines
