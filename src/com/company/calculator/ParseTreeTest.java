package com.company.calculator;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;

public class ParseTreeTest {
    public static void main(String[] args) throws IOException {
        ANTLRFileStream input = new ANTLRFileStream("simple_let_test.txt"); // a character stream
        ExpLexer lex = new ExpLexer(input); // transforms characters into tokens
        CommonTokenStream tokens = new CommonTokenStream(lex); // a token stream
        ExpParser parser = new ExpParser(tokens); // transforms tokens into parse trees
        ParseTree parseTree = parser.expression(); // creates the parse tree from the called rule

        ExpressionEvaluatorVisitor evaluator = new ExpressionEvaluatorVisitor();
        System.out.println("the output is " + evaluator.visit(parseTree));
    }
}
